# Rainverse Wiki extra maintenance

This is a script that checks an XML backup of the [Rainverse Wiki] for
consistency, more specifically, it checks:
* That all character pages have at least one "XYZ characters" category
* That all St. Hallvard students and staff character pages have their
  respective St. Hallvard categories
* That all character pages do not have "\*-Human" set as a species (but
  rather "\*-human")
* That all comic files have "{{Comic}}"
* That all pages follow [WP:REFPUNC]
* That all pages don't link to comic pages through old-style internal links
* That all transcripts have their talk templates on separate lines
* That there are no duplicate references on a page

This is _not_ a substitute for [Category:Maintenance], but rather something
extra.

[Rainverse Wiki]: https://rainverse.wiki
[WP:REFPUNC]: https://en.wikipedia.org/wiki/WP:REFPUNC
[Category:Maintenance]: https://rainverse.wiki/wiki/Category:Maintenance
