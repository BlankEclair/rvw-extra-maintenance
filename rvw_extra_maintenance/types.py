from dataclasses import dataclass

@dataclass
class Revision:
    id: int = None
    model: str = None
    format: str = None
    text: str = None

@dataclass
class Page:
    title: str = None
    namespace_id: int = None
    namespace_name: str | None = None
    id: int = None
    redirect: str | None = None
    latest_revision: Revision = None
