import os
import pdb
import sys
import gzip
import typing
import argparse
import importlib
import traceback

from . import ASSERT_CALLBACK, test_callbacks, Page
from .parser import parse_file

# === PARSE ARGUMENTS ===
parser = argparse.ArgumentParser(prog="rvw_extra_maintenance")
parser.add_argument("--ignore-page", "-i", action="append", default=[], help="test condition to ignore (page test) (function name,revision id)")
parser.add_argument("--ignore-post", action="append", default=[], help="test condition to ignore (post test) (function name)")
parser.add_argument("--continue", "-c", action="store_true", help="continue executing tests even after failures")
parser.add_argument("--verbose", "-v", action="store_true", help="print traceback when assertion fails")
parser.add_argument("--pdb", "-p", action="store_true", help="activates pdb (python debugger) when an assertion fails")
parser.add_argument("filename", help="gzipped xml backup to check")
args = parser.parse_args()

page_tests_to_ignore = {
    (name, int(id)) for name, id in (i.split(",") for i in args.ignore_page)
}
post_tests_to_ignore = args.ignore_post

# === IMPORT TESTS ===
tests_dir = os.path.join(os.path.dirname(__file__), "tests")
for filename in sorted(os.listdir(tests_dir)):
    filename = filename.removesuffix(".py")
    importlib.import_module(f".{filename}", f"{__package__}.tests")

# === ASSERT CALLBACK BOILERPLATE ===
def skip_frame(frame: traceback.FrameSummary) -> bool:
    return frame.filename == __file__ or \
            (frame.filename.startswith("<") and frame.filename.endswith(">"))
def extract_partial_stack() -> traceback.StackSummary:
    stack = traceback.extract_stack()
    while skip_frame(stack[-1]):
        stack.pop()
    while skip_frame(stack[0]):
        stack = stack[1:]
    return stack

tests_failed = False
def make_asrt(function_name: str, ignore_key: str) -> ASSERT_CALLBACK:
    def asrt(condition: bool, message: str | None = None):
        global tests_failed
        if condition:
            return

        text = f'Check "{ignore_key}" failed'
        if message:
            text = f"{text}: {message}"
        if args.verbose:
            stack = extract_partial_stack()
            print("Traceback (most recent call last):\n", *traceback.format_list(stack), sep="", end="", file=sys.stderr)
        print(text, file=sys.stderr)
        if args.pdb:
            pdb.Pdb(skip=["__main__"]).set_trace()

        tests_failed = True
        if not getattr(args, "continue"):
            sys.exit(1)

    return asrt

# === RUN PAGE TESTS ===
def _handle_page_test(page: Page, test: typing.Callable[[ASSERT_CALLBACK, Page], None]):
    ignore_key = (test.__name__, page.latest_revision.id)
    if ignore_key in page_tests_to_ignore:
        return

    test(make_asrt(test.__name__, f"{test.__name__},{page.latest_revision.id}"), page)
with gzip.open(args.filename) as file:
    for page in parse_file(file):
        for test in test_callbacks["page"]:
            _handle_page_test(page, test)

# === RUN POST TESTS ===
for test in test_callbacks["post"]:
    if test.__name__ in post_tests_to_ignore:
        continue

    test(make_asrt(test.__name__, test.__name__))

# === EXIT WITH 1 IF TESTS FAILED ===
if tests_failed:
    sys.exit(1)
