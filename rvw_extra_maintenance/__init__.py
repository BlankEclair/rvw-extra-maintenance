import typing
from .types import Page, Revision

ASSERT_CALLBACK = typing.Callable[[bool, str | None], None]

test_callbacks: dict[str, list] = {
    "page": [],
    "post": [],
}

def register_test(stage: str):
    def wrapper(callback):
        test_callbacks[stage].append(callback)
        return callback

    return wrapper
