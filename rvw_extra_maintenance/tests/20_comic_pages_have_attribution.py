"""
This test ensures that comic pages have {{Comic}} in their description
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

PAGEBOX_RE = re.compile(r"\{\{(?:\s*[Tt]emplate:)?[Pp]agebox")
IMAGE_RE = re.compile(r"\|\s*image\s*=\s*([^|}]+?)\s*[\|}]")
ATTRIBUTION_RE = re.compile(r"\{\{Comic(?:\}\}|\|)")

comic_image_files = {}

def _handle_file_page(page: Page):
    if not ATTRIBUTION_RE.search(page.latest_revision.text):
        return

    comic_image_files[page.title.partition(":")[2]] = True

def _handle_comic_page_page(page: Page):
    if not PAGEBOX_RE.search(page.latest_revision.text):
        return

    match = IMAGE_RE.search(page.latest_revision.text)
    image_name = match.group(1).replace("_", " ")
    if image_name not in comic_image_files:
        comic_image_files[image_name] = False

@register_test("page")
def comic_pages_have_attribution(_, page: Page):
    if page.namespace_id == 6:
        _handle_file_page(page)
    elif page.namespace_name in ("Rain", "MIS", "Rainbow"):
        _handle_comic_page_page(page)

@register_test("post")
def check_comic_pages_have_attribution(asrt: ASSERT_CALLBACK):
    for image, has_attribution in comic_image_files.items():
        asrt(has_attribution, f'"File:{image}" is missing {{{{Comic}}}}')
