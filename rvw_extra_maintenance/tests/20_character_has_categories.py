"""
This test checks if character pages have:
* At least one "XYZ characters" category if applicable
* A "St. Hallvard students/staff" category if applicable
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

TEMPLATE_RE = re.compile(r"\{\{(?:\s*[Tt]emplate:)?[Ii]nfobox[ _]character")
CHARACTERS_CATEGORY_RE = re.compile(r"\[\[[Cc]ategory:[\w\s]+ characters\]\]")
# Unsure how to handle Black Wings, Kaminari characters for now (put in Rain? MIS? Neither?), so ignore them for the time being
# Update: Some members on Kaminari's team are now categorized as "Otherworlders" for now
BWK_CATEGORY_RE = re.compile(r"\[\[[Cc]ategory:(Black Wings, Kaminari|Otherworlders)\]\]")

@register_test("page")
def check_character_has_characters_categories(asrt: ASSERT_CALLBACK, page: Page):
    if page.namespace_id != 0:
        return
    if not TEMPLATE_RE.search(page.latest_revision.text):
        return

    asrt(CHARACTERS_CATEGORY_RE.search(page.latest_revision.text) or BWK_CATEGORY_RE.search(page.latest_revision.text),
            f'"{page.title}" is missing a "XYZ characters" category')

AFFILIATION_RE = re.compile(r"\|\s*affiliation\s*=\s*([^|}]+)")

@register_test("page")
def check_character_has_st_hallvard_categories(asrt: ASSERT_CALLBACK, page: Page):
    if page.namespace_id != 0:
        return
    if not TEMPLATE_RE.search(page.latest_revision.text):
        return

    match = AFFILIATION_RE.search(page.latest_revision.text)
    if not match:
        return

    affiliation = match.group(1).lower()
    if "st. hallvard" not in affiliation:
        return

    if "student" in affiliation:
        asrt("[[Category:St. Hallvard students]]" in page.latest_revision.text,
                f'"{page.title}" is missing a "[[Category:St. Hallvard students]]"')
    else:
        asrt("[[Category:St. Hallvard staff]]" in page.latest_revision.text,
                f'"{page.title}" is missing a "[[Category:St. Hallvard staff]]"')
