"""
This test ensures that character pages don't have their species set as "*-Human"
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

TEMPLATE_RE = re.compile(r"\{\{(?:\s*[Tt]emplate:)?[Ii]nfobox[ _]character")
BAD_SPECIES_RE = re.compile(r"\|\s*species\s*=\s*\w+-Human")

@register_test("page")
def check_character_species_capitalization(asrt: ASSERT_CALLBACK, page: Page):
    if page.namespace_id != 0:
        return
    if not TEMPLATE_RE.search(page.latest_revision.text):
        return

    asrt(not BAD_SPECIES_RE.search(page.latest_revision.text), f'"{page.title}" is a "*-Human" species')
