"""
This test ensures that pages do not have duplicate references
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

REF_RE = re.compile(r"<ref(?:\s[^/>]*)?>([\s\S]+?)</ref>", re.I)

@register_test("page")
def no_duplicate_refs(asrt: ASSERT_CALLBACK, page: Page):
    refs = [i.group(1) for i in REF_RE.finditer(page.latest_revision.text)]
    asrt(len(refs) == len(set(refs)), f'"{page.title}" has duplicate references')
