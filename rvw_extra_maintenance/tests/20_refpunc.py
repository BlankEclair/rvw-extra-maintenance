"""
This test ensures that pages follow WP:REFPUNC
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

BAD_REFPUNC_RE = re.compile(r"(<ref[^>]*?(>.+?</ref|/)>|{{(Template:)?(Citation[ _]needed|Fact|Better[ _]source[ _]needed|Bcn|BSN|Better[ _]source)(\|[^}]*)?}})[,.;:]", re.I)

@register_test("page")
def refpunc(asrt: ASSERT_CALLBACK, page: Page):
    asrt(not BAD_REFPUNC_RE.search(page.latest_revision.text), f'"{page.title}" does not follow WP:REFPUNC')
