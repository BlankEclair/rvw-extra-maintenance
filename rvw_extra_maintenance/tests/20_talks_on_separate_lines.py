"""
This test ensures that each {{Talk}} template is on its own line
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

BAD_TALK_RE = re.compile(r"\}\} *\{\{([Tt]emplate:)?[Tt]alk/")

@register_test("page")
def talks_on_separate_lines(asrt: ASSERT_CALLBACK, page: Page):
    asrt(not BAD_TALK_RE.search(page.latest_revision.text), f'"{page.title}" does not have {{{{Talk}}}} templates on separate lines')
