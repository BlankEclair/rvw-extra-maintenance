"""
This test checks if the types are properly filled in
"""
from .. import ASSERT_CALLBACK, Page, register_test

@register_test("page")
def check_internal_type_consistency(asrt: ASSERT_CALLBACK, page: Page):
    asrt(isinstance(page.title, str))
    asrt(isinstance(page.namespace_id, int))
    asrt(isinstance(page.namespace_name, str) or page.namespace_id == 0)
    asrt(isinstance(page.id, int))

    asrt(isinstance(page.latest_revision.id, int))
    asrt(isinstance(page.latest_revision.model, str))
    asrt(isinstance(page.latest_revision.format, str))
    asrt(isinstance(page.latest_revision.text, str))
