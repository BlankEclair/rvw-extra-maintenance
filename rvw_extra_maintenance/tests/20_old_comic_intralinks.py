"""
This test ensures that internal wiki links to pages are to the proper namespaces
"""
import re
from .. import ASSERT_CALLBACK, Page, register_test

BAD_PAGE_RE = re.compile(r"\[\[\d")
@register_test("page")
def bad_page_link_check(asrt: ASSERT_CALLBACK, page: Page):
    asrt(not BAD_PAGE_RE.search(page.latest_revision.text), f'"{page.title}" uses old-style internal links to comic pages')

BAD_CHAPTER_RE = re.compile(r"\[\[[Cc]hapter[ _]")
# Workaround for "Chapter 1" redirecting to "Chapter 1 (disambiguation)"
# (Intentionally too lazy to handle _ since that honestly is just bad style that technically works)
DISAMBIGUATION_REDIRECT_RE = re.compile(r"#REDIRECT \[\[Chapter \d+ \(disambiguation\)\]\]", re.I)
@register_test("page")
def bad_chapter_link_check(asrt: ASSERT_CALLBACK, page: Page):
    text = page.latest_revision.text
    asrt(
        not BAD_CHAPTER_RE.search(text) or DISAMBIGUATION_REDIRECT_RE.match(text),
        f'"{page.title}" uses old-style internal links to chapters',
    )

BAD_VOLUME_RE = re.compile(r"\[\[[Vv]olume[ _]")
@register_test("page")
def bad_volume_link_check(asrt: ASSERT_CALLBACK, page: Page):
    asrt(not BAD_VOLUME_RE.search(page.latest_revision.text), f'"{page.title}" uses old-style internal links to volumes')
