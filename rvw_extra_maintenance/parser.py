import typing
from dataclasses import dataclass, field
import xml.etree.ElementTree as ElementTree
from . import Page, Revision

PREFIX = "{http://www.mediawiki.org/xml/export-0.11/}"
@dataclass
class MachineData:
    machine: typing.Callable[[str, ElementTree.Element, "MachineData"], typing.Any]

    namespace_id_to_name: dict[int, str] = field(default_factory=dict)
    page: Page | None = None
    revision: Revision | None = None


def _main_machine(event: str, element: ElementTree.Element, data: MachineData):
    if event == "start" and element.tag == f"{PREFIX}namespaces":
        data.machine = _namespaces_machine
    elif event == "start" and element.tag == f"{PREFIX}page":
        data.machine = _page_machine
        data.page = Page()

def _namespaces_machine(event: str, element: ElementTree.Element, data: MachineData):
    if event == "end" and element.tag == f"{PREFIX}namespace":
        namespace_id = int(element.get("key"))
        namespace_name = element.text

        data.namespace_id_to_name[namespace_id] = namespace_name
    elif event == "end" and element.tag == f"{PREFIX}namespaces":
        data.machine = _main_machine

def _page_machine(event: str, element: ElementTree.Element, data: MachineData) -> Page | None:
    if event == "end" and element.tag == f"{PREFIX}title":
        data.page.title = element.text
    elif event == "end" and element.tag == f"{PREFIX}ns":
        data.page.namespace_id = int(element.text)
        data.page.namespace_name = data.namespace_id_to_name[data.page.namespace_id]
    elif data.page.id is None and event == "end" and element.tag == f"{PREFIX}id":
        data.page.id = int(element.text)
    elif event == "end" and element.tag == f"{PREFIX}redirect":
        data.page.redirect = element.get("title")
    elif event == "start" and element.tag == f"{PREFIX}revision":
        data.machine = _revision_machine
        data.revision = Revision()
    elif event == "end" and element.tag == f"{PREFIX}page":
        page = data.page
        data.machine = _main_machine
        data.page = None
        return page

def _revision_machine(event: str, element: ElementTree.Element, data: MachineData):
    if data.revision and data.revision.id is None and event == "end" and element.tag == f"{PREFIX}id":
        data.revision.id = int(element.text)
        if data.page.latest_revision and data.page.latest_revision.id > data.revision.id:
            data.revision = None
    elif data.revision and event == "end" and element.tag == f"{PREFIX}model":
        data.revision.model = element.text
    elif data.revision and event == "end" and element.tag == f"{PREFIX}format":
        data.revision.format = element.text
    elif data.revision and event == "end" and element.tag == f"{PREFIX}text":
        data.revision.text = element.text or ""
    elif event == "end" and element.tag == f"{PREFIX}revision":
        if data.revision:
            data.page.latest_revision = data.revision
            data.revision = None
        data.machine = _page_machine


def parse_file(file: typing.TextIO):
    data = MachineData(_main_machine)

    for event, element in ElementTree.iterparse(file, ("start", "end")):
        result = data.machine(event, element, data)
        if result is not None:
            yield result
